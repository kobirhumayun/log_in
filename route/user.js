const express = require('express')
const route = express.Router()
const {loggedin} = require('../controller/accessControl')

// Log in middleware
// route sample /user/q?username=user2&pass=123452&role=general
route.use(loggedin)
route.get("/q",(req, res)=>{
    res.status(200).send("you are successfully loged in")
})
module.exports = route