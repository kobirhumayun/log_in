const express = require('express')
const {
    loggedin
} = require('../controller/accessControl')
const {
    filterBy
} = require('../controller/query')

const route = express.Router()

// sample url localhost:3000/products/q?username=user2&pass=123452&role=general&category=electronics
route.use(loggedin, filterBy)
route.get("/q", (req, res)=>{
    res.status(200).send(res.filterData)
    
})


module.exports = route