const express = require('express')
const app = express()
const port = 3000
const usersRoute = require('./route/user')
const productsRoute = require('./route/products')
// sample url localhost:3000/products/q?username=user2&pass=123452&role=general&category=electronics
app.use('/user', usersRoute)
app.use('/products', productsRoute)




app.listen(port)