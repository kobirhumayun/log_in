const products = require('../data/producsData')
const filterBy = (req, res, next) => {
    let filterData = products.filter(e => {
        return e.category === req.query.category
    })
    res.filterData = filterData
    next()

}
module.exports = {
    filterBy
}