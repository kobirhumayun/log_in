const usersData = require("../data/usersData")


const loggedin = (req, res, next) => {
    const userInformation = usersData.filter(e => {
        return e.username === req.query.username
    })
    if (userInformation.length) {
        if (userInformation[0].username === req.query.username && userInformation[0].password === req.query.pass) {
            next()
        } else {
            res.send("Wrong password")
        }
    } else {
        res.send("No user found")
    }
}



module.exports = {
    loggedin
}